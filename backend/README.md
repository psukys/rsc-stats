# RSC Statistics backend
Server side logic responsible for:
 - Retrieving data from zybez.net,
 - Caching essentials into database,
 - Making custom analysis on data available,
 - Providing REST API access to frontend
